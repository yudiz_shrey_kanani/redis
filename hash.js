const Redis = require("ioredis");
const redis = new Redis();

// hashes

redis.hmset("stu-3", "name", "shrey", "age", 20, "class", 10);
redis.hexists("stu-3", "name", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("hexist name = " + data);
  }
});
redis.hexists("stu-3", "name1", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("hexist name1 = " + data);
  }
});

redis.hsetnx("stu-3", "name", "rudra", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("hsetnx name = " + data);
  }
});
redis.hsetnx("stu-3", "surname", "kanani", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log("hsetnx surname = " + data);
  }
});

redis.hkeys("stu-3", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.hvals("stu-3", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.hgetall("stu-3", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("hgetall");
    console.log(data);
  }
});

redis.hlen("stu-3", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("hgetall");
    console.log(data);
  }
});
redis.hmget("stu-3", "name", "surname", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("hgetall");
    console.log(data);
  }
});
redis.hincrby("stu-3", "age", 5, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log("hgetall");
    console.log(data);
  }
});
