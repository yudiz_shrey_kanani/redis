const Redis = require("ioredis");
const redis = new Redis();

redis.lpush("country2", "india");
redis.lpush("country2", "usa");
redis.lpush("country2", "china");

redis.lrange("country2", "0", "-1", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});

redis.rpush("country2", "england");

redis.llen("country2", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});

redis.rpop("country2", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});

redis.lset("country2", 0, "uae");

redis.sort("country2", "alpha", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
