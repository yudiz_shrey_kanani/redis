const Redis = require("ioredis");
const redis = new Redis();

// sets
redis.sadd("myset1", 1, 2, 3, 4, 5);
redis.sadd("myset2", 4, 5, 6, 7, 8, 9);
redis.sdiff("myset1", "myset2", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
redis.sdiffstore("resultset1", "myset1", "myset2");
redis.sunion("myset1", "myset2", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
redis.sunionstore("resultset2", "myset1", "myset2");
redis.sinter("myset1", "myset2");
redis.spop("myset2", 5);

redis.smembers("myset1", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
redis.smembers("myset2", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
redis.smembers("resultset1", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
redis.smembers("resultset2", function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
});
