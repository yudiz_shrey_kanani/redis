const Redis = require("ioredis");
const redis = new Redis();

redis.zadd("sortedset2", 1, "a", 2, "b");
redis.zrange("sortedset2", 0, -1, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zcard("sortedset2", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zcount("sortedset2", 0, 1, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zadd("sortedset2", 5, "d");
redis.zrange("sortedset2", 0, -1, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zrem("sortedset2", "d");

redis.zrange("sortedset2", 0, -1, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zrank("sortedset2", "b", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zrevrank("sortedset2", "b", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zscore("sortedset2", "b", function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
redis.zrangebyscore("sortedset2", 0, 3, function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
