const Redis = require("ioredis");
const redis = new Redis();

// string
redis.set("foo", "bar");
redis.set("age1", 20);
redis.incr("age1");
redis.getrange("foo", "0", "1");
redis.set("test", 5);
redis.expire("test", 10);

redis.setnx("foo", "bar1");
redis.setnx("foo1", "bar1");

redis.append("foo", " shrey kanani");

redis.get("foo", function (err, result) {
  if (err) {
    console.error(err);
  } else {
    console.log(result);
  }
});

redis.get("age1", function (err, result) {
  if (err) {
    console.error(err);
  } else {
    console.log(result);
  }
});

redis.get("email", function (err, result) {
  if (err) {
    console.error(err);
  } else {
    console.log(result);
  }
});
